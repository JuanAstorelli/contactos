<%@page contentType="text/html;charset=UTF-8" %>

<div class="modal" id="modalDel" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Eliminar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>¿Seguro que deseas eliminar este elemento?</p>
      </div>
      <div class="modal-footer">
        <a id="confirmaDel" class="btn btn-primary">Sí</a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<script>
function delContacto(id) {

  $("#confirmaDel").attr("href", "/contactos/contacto/elimina.jsp?id=" + id);
}

function delLlamada(id, contacto) {

  $("#confirmaDel").attr("href", "/contactos/llamada/elimina.jsp?id=" + id + "&contacto=" + contacto);
}
</script>