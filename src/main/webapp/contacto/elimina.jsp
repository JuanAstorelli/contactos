<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.businessLogic.*" %>

<%
    String id = request.getParameter("id");
    if (id==null) {
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/contactos/index.jsp");
    } else {
        int id_numerico = Integer.parseInt(id);
        Controller.removeId(id_numerico);
        response.sendRedirect("/contactos/contacto/list.jsp");
    }
%>
