<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.businessLogic.*" %>

<%
    Contacto con = null;
    boolean callOk = false;
    if ("GET".equalsIgnoreCase(request.getMethod())) {

        String id = request.getParameter("contacto");
        if (id != null) {

          callOk = true;

          con = Controller.getContactById(Integer.parseInt(id));
        }
    }

    if (!callOk) {

        response.sendRedirect("/contactos/contacto/list.jsp");
        return;
    }
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ContactosApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>

<body>

<%@include file="/menu.jsp"%>

<div class="container">

<div class="row">
  <div class="col">
    <h1>Llamadas de <%= con.getNombre() %></h1>
  </div>
</div>

<div class="row">
<div class="col">

<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Destinatario</th>
      <th scope="col">Fecha</th>
      <th scope="col">Notas</th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>

   <% for (Llamada llam : Controller.getLlamadasByContactoId(con.getId())) { %>
      <tr>
        <th scope="row"><%= llam.getId() %></th>
        <td>
          <% Contacto dest = Controller.getContactById(llam.getDestinatario()); %>
          <%= dest.getNombre() %>(<%= dest.getTelf() %>)
        </td>
        <td><%= llam.getFecha().toString() %></td>
        <td><%= llam.getNotas() %></td>
        <td><a href="/contactos/llamada/edita.jsp?con=<%= con.getId() %>&llam=<%= llam.getId() %>">Edita</a></td>
        <td>
          <button onclick="delLlamada(<%= llam.getId() %>, <%= con.getId() %>)" type="button" class="btn btn-link" data-toggle="modal" data-target="#modalDel">Eliminar</button>
        </td>
      </tr>
    <% } %>
  </tbody>
</table>

<%@include file="/dialogoDel.jsp"%>

<a href="/contactos/llamada/crea.jsp?contacto=<%= con.getId() %>" class="btn btn-sm btn-danger">Nueva Llamada</a>

</div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/contactos/js/scripts.js"></script>

</body>
</html>