<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.businessLogic.*" %>

<%
    String id = request.getParameter("id");
    if (id==null) {
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/contactos/index.jsp");
    } else {
        int id_numerico = Integer.parseInt(id);
        int contacto = Integer.parseInt(request.getParameter("contacto"));
        Controller.removeLlamadaId(id_numerico);
        response.sendRedirect("/contactos/llamada/lista.jsp?contacto=" + contacto);
    }
%>
