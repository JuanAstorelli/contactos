package com.enfocat.mvc.backend;

import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.enfocat.mvc.businessLogic.Llamada;

public class LlamadaMySQL extends MySQLGeneralController implements LlamadaDAO {

    public LlamadaMySQL(String table, String key) {

        super(table, key);
    }

    @Override
    public Llamada newLlamada(Llamada llam) {

        String sql = String.format("INSERT INTO %s (remitente, destinatario, fecha, notas) VALUES (?, ?, ?, ?)", table);

        try (Connection conn = getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {

            pstmt.setInt(1, llam.getRemitente());
            pstmt.setInt(2, llam.getDestinatario());

            long ms = llam.getFecha().getTime();
            java.sql.Date fecha_sql = new java.sql.Date(ms);
            pstmt.setDate(3, fecha_sql);

            pstmt.setString(4, llam.getNotas());
            pstmt.executeUpdate();

            ResultSet rs = stmt.executeQuery("select last_insert_id()");
            if (rs.next()) {
                llam.setId(rs.getInt(1));
            }
        } catch (Exception e) {

            System.out.println(e.getMessage());
        }

        return llam;
    }

    @Override
    public boolean deleteLlamadaPorId(int id) {
        
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s = %d", table, key, id);
        try (Connection conn = getConn();
                Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(sql);
            deleted=true;
        }
        catch (Exception e) {

            System.out.println(e.getMessage());
        }
        
        return deleted;
    }

    @Override
    public List<Llamada> getLlamadasContactoId(int id) {
        
        List<Llamada> llamadas = new ArrayList<Llamada>();

        String sql = String.format("select %s, remitente, destinatario, fecha, notas from %s where remitente = %d", key, table, id);

        try (Connection conn = getConn();
            Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {

                Llamada llamAux = new Llamada(
                                            rs.getInt(key),
                                            rs.getInt("remitente"),
                                            rs.getInt("destinatario"),
                                            rs.getDate("fecha"),
                                            rs.getString("notas")
                                            );
                llamadas.add(llamAux);
            }
        }
        catch (Exception e) {

            String s = e.getMessage();
            System.out.println(s);
        }

        return llamadas;
    }

    @Override
    public Llamada getLlamadaById(int id) {
        
        Llamada llamada = null;

        String sql = String.format("select %s, remitente, destinatario, fecha, notas from %s where %s = %d", key, table, key, id);

        try (Connection conn = getConn();
            Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {

                llamada = new Llamada(
                                            rs.getInt(key),
                                            rs.getInt("remitente"),
                                            rs.getInt("destinatario"),
                                            rs.getDate("fecha"),
                                            rs.getString("notas")
                                            );
            }
        }
        catch (Exception e) {

            String s = e.getMessage();
            System.out.println(s);
        }

        return llamada;
    }

    @Override
    public boolean updateLlamada(Llamada llam) {
        
        boolean updated = false;
        String sql = String.format("UPDATE %s set notas=? where %s=%d", table, key, llam.getId());
    
        try (Connection conn = getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            
            pstmt.setString(1, llam.getNotas());
            pstmt.executeUpdate(); 
            updated = true;
        
        } catch (Exception e) {

            System.out.println(e.getMessage());
        }

        return updated;
    }

}