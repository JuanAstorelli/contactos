package com.enfocat.mvc.backend;

import com.enfocat.mvc.businessLogic.*;

import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ContactoMySQL extends MySQLGeneralController implements ContactoDAO {

    public ContactoMySQL(String table, String key) {

        super(table, key);
    }

    @Override
    public Contacto newContacto(Contacto cn) {
        
        String sql = String.format("INSERT INTO %s (nombre, email, telefono, ciudad, urlfoto) VALUES (?, ?, ?, ?, ?)", table);

        try (Connection conn = getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {

            pstmt.setString(1, cn.getNombre());
            pstmt.setString(2, cn.getEmail());
            pstmt.setString(3, cn.getTelf());
            pstmt.setString(4, cn.getCiudad());
            pstmt.setString(5, cn.getUrlfoto());
            pstmt.executeUpdate();

            ResultSet rs = stmt.executeQuery("select last_insert_id()");
            if (rs.next()) {
                cn.setId(rs.getInt(1));
            }
        }
        catch (Exception e) {

            System.out.println(e.getMessage());
        }

        return cn;
    }

    @Override
    public List<Contacto> getContactos() {

        List<Contacto> listaContactos = new ArrayList<Contacto>();
        String sql = String.format("select %s, nombre, email, telefono, ciudad, urlfoto from %s", key, table);
        try (Connection conn = getConn();
            Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Contacto u = new Contacto(rs.getInt(key),
                                        rs.getString("nombre"),
                                        rs.getString("email"),
                                        rs.getString("telefono"),
                                        rs.getString("ciudad"),
                                        rs.getString("urlfoto"));

                listaContactos.add(u);
            }

        }
        catch (Exception e) {

            String s = e.getMessage();
            System.out.println(s);
        }

        return listaContactos;
    }

    @Override
    public Contacto getContactoById(int id) {

        Contacto cn = null;
        String sql = String.format("select %s, nombre, email, telefono, ciudad, urlfoto from %s where %s = %d", key, table, key, id);
        try (Connection conn = getConn();
            Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                cn = new Contacto(
                        rs.getInt(key), 
                        rs.getString("nombre"),
                        rs.getString("email"),
                        rs.getString("telefono"),
                        rs.getString("ciudad"),
                        rs.getString("urlfoto"));
            }
        }
        catch (Exception e) {

            String s = e.getMessage();
            System.out.println(s);
        }

        return cn;
    }

    @Override
    public boolean updateContacto(Contacto cn) {

        boolean updated = false;
        String sql = String.format("UPDATE contactos set nombre = ?, email = ?, telefono = ?, ciudad = ?, urlfoto = ? where %s = %d", key, cn.getId());
    
        try (Connection conn = getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            
            pstmt.setString(1, cn.getNombre());
            pstmt.setString(2, cn.getEmail());
            pstmt.setString(3, cn.getTelf());
            pstmt.setString(4, cn.getCiudad());
            pstmt.setString(5, cn.getUrlfoto());
            pstmt.executeUpdate(); 
            updated = true;
        
        }
        catch (Exception e) {

            System.out.println(e.getMessage());
        }

        return updated;
    }

    @Override
    public boolean deleteContactoById(int id) {

        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", table, key, id);
        try (Connection conn = getConn();
                Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(sql);
            deleted=true;
        }
        catch (Exception e) {

            System.out.println(e.getMessage());
        }
        
        return deleted;
    }
}