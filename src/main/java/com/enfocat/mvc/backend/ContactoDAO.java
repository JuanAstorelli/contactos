package com.enfocat.mvc.backend;

import com.enfocat.mvc.businessLogic.*;

import java.util.List;
import com.enfocat.mvc.*;

public interface ContactoDAO {

    public Contacto newContacto(Contacto cn);
    public List<Contacto> getContactos();
    public Contacto getContactoById(int id);
    public boolean updateContacto(Contacto cn);
    public boolean deleteContactoById(int id);
}