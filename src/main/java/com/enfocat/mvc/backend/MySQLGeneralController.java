package com.enfocat.mvc.backend;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public abstract class MySQLGeneralController {

    private static final String URL = "jdbc:mysql://localhost/javacontactos";
	private static final String USERNAME = "root";
    private static final String PASSWORD = "";
    
    protected String table;
    protected String key;

    protected MySQLGeneralController(String table, String key) {

        this.table = table;
        this.key = key;
    }

	protected static Connection getConn() throws SQLException {

		DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		return (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}
}