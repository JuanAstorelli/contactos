package com.enfocat.mvc.backend;

import com.enfocat.mvc.businessLogic.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Datos implements ContactoDAO, LlamadaDAO {
    
    private List<Contacto> contactos;
    private List<Llamada> llamadas;
    private int ultimoContacto;
    private int ultimaLlamada;

    public Datos() {

        this.contactos = new ArrayList<Contacto>();
        this.llamadas = new ArrayList<Llamada>();
        this.ultimoContacto = 0;
        this.ultimaLlamada = 0;

        contactos.add(new Contacto(1,"ana", "ana@gmail.com", "676543345", "Pandora", "avatar1.jpg"));
        contactos.add(new Contacto(2,"joan", "joan@gmail.com", "634122344", "Pandora", "avatar2.jpg"));
        ultimoContacto  =2;

        llamadas.add(new Llamada(1, 1, 2, new Date(2017, 6, 3), "reunion1"));
        llamadas.add(new Llamada(2, 1, 2, new Date(2017, 6, 6), "reunion2"));
        llamadas.add(new Llamada(3, 2, 1, new Date(2017, 6, 5), "colegueo"));
        ultimaLlamada = 3;
    }

    @Override
    public Llamada newLlamada(Llamada llam) {
        
        ultimaLlamada++;
        llam.setId(ultimaLlamada);
        llamadas.add(llam);
        return llam;
    }

    @Override
    public boolean deleteLlamadaPorId(int id) {
        
        boolean deleted = false;
        for (Llamada llam : llamadas) {

            if (llam.getId() == id) {

                llamadas.remove(llam);
                deleted = true;
                break;
            }
        }

        return deleted;
    }

    @Override
    public List<Llamada> getLlamadasContactoId(int id) {
        
        List<Llamada> llamsCon = new ArrayList<Llamada>();
        for (Llamada llam : llamadas) {
            System.out.println("entra for" + llam.getRemitente());
            if (llam.getRemitente() == id) {
                System.out.println("entra if");
                llamsCon.add(llam);
                System.out.println(llam.getRemitente());
            }
        }

        return llamsCon;
    }

    @Override
    public Contacto newContacto(Contacto cn) {
        
        ultimoContacto++;
        cn.setId(ultimoContacto);
        contactos.add(cn);
        return cn;
    }

    @Override
    public List<Contacto> getContactos() {
        
        return contactos;
    }

    @Override
    public Contacto getContactoById(int id) {
        
        for (Contacto cn : contactos){
            if (cn.getId()==id){
                return cn;
            }
        }
        return null;
    }

    @Override
    public boolean updateContacto(Contacto cn) {
        
        boolean updated = false;
        for (Contacto x : contactos){
            if (cn.getId()==x.getId()){
                //x = cn;
                int idx = contactos.indexOf(x);
                contactos.set(idx,cn);
                updated=true;
                break;
            }
        }
        return updated;
    }

    @Override
    public boolean deleteContactoById(int id) {
        
        boolean deleted = false;
        
        for (Contacto x : contactos){
            if (x.getId()==id){
                contactos.remove(x);
                deleted=true;
                break;
            }
        }
        return deleted;
    }

    @Override
    public Llamada getLlamadaById(int id) {
        
        for (Llamada llamada : llamadas) {

            if (llamada.getId() == id) {

                return llamada;
            }
        }

        return null;
    }

    @Override
    public boolean updateLlamada(Llamada llam) {

        Llamada updating = getLlamadaById(llam.getId());
        updating.setNotas(llam.getNotas());
        return true;
    }
}

