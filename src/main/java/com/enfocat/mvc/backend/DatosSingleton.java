package com.enfocat.mvc.backend;

public class DatosSingleton {

    private static Datos backendMem = null;

    public static Datos getInstance() {

        if (backendMem == null) {

            backendMem = new Datos();
        }

        return backendMem;
    }

    /** ¿Quina és la diferència?
    private static Datos backendMem = new Datos();

    public static Datos getInstance() {

        return backendMem;
    }
    */
}