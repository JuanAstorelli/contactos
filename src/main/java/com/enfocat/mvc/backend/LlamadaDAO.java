package com.enfocat.mvc.backend;

import com.enfocat.mvc.businessLogic.*;
import java.util.List;

public interface LlamadaDAO {

    Llamada newLlamada(Llamada llam);
    boolean deleteLlamadaPorId(int id);
    boolean updateLlamada(Llamada llam);
    List<Llamada> getLlamadasContactoId(int id);
	Llamada getLlamadaById(int id);
}