package com.enfocat.mvc.businessLogic;

import com.enfocat.mvc.backend.*;

import java.util.List;

public class Controller {

    private static ContactoDAO contDao = new ContactoMySQL("contactos", "idcontactos");
    private static LlamadaDAO llamDao = new LlamadaMySQL("llamadas", "idllamadas");
    
    // ó
    //private static ContactoDAO contDao = DatosSingleton.getInstance();
    //private static LlamadaDAO llamDao = DatosSingleton.getInstance();

    // getAll devuelve la lista completa
    public static List<Contacto> getAll() {

        return contDao.getContactos();
    }

    // getId devuelve un registro
    public static Contacto getContactById(int id) {

        return contDao.getContactoById(id);
    }

    public static List<Llamada> getLlamadasByContactoId(int id) {

        return llamDao.getLlamadasContactoId(id);
    }

    public static Llamada getLlamadaById(int id) {

        return llamDao.getLlamadaById(id);
    }

    // save guarda un Contacto
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Contacto cn) {

        if (cn.getId() > 0) {

            contDao.updateContacto(cn);
        } else {

            contDao.newContacto(cn);
        }
    }

    public static void guardaLlamada(Llamada llam) {

        if (llam.getId() > 0) {

            llamDao.updateLlamada(llam);
        }
        else {

            llamDao.newLlamada(llam);
        }
    }

    // size devuelve numero de Contactos
    public static int size() {

        return contDao.getContactos().size();
    }

    // removeId elimina Contacto por id
    public static void removeId(int id) {

        contDao.deleteContactoById(id);
    }

    public static void removeLlamadaId(int id) {

        llamDao.deleteLlamadaPorId(id);
    }

    // asignaFoto asigna URLde FOTO a alumno por id
    public static void setUrlfoto(int id, String url) {

        Contacto updating = Controller.getContactById(id);
        updating.setUrlfoto(url);
        contDao.updateContacto(updating);
    }

    // asignaFoto asigna URLde FOTO a alumno por id
    public static String getUrlfoto(int id) {

        return Controller.getContactById(id).getUrlfoto();
    }

    public static void updateLlamada(int id, String notas) {

        Llamada toUpdate = getLlamadaById(id);
        toUpdate.setNotas(notas);
        guardaLlamada(toUpdate);
    }
}