package com.enfocat.mvc.businessLogic;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MiLib{

    public static Date stringToDate(String sdata){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        Date theDate = null;
            try {
                theDate = df.parse(sdata);
            } catch (ParseException e) {
                //e.printStackTrace();
            }
        return theDate;
    }
}
