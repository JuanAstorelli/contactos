package com.enfocat.mvc.businessLogic;

public class Contacto {
  
    private int id;
    private String nombre;
    private String email;

    private String telf;
    private String ciudad;

    private String urlfoto;
   
    public Contacto(String nombre, String email) {
        this.id=0;
        this.nombre = nombre;
        this.email = email;
    }

    // CONSTRUCTOR SIN ID
    public Contacto(String nombre, String email, String telf, String ciudad) {
        this.id=0;
        this.nombre = nombre;
        this.email = email;

        this.telf = telf;
        this.ciudad = ciudad;
    }
   
    //CONSTRUCTOR CON ID
    public Contacto(int id, String nombre, String email, String telf, String ciudad, String urlfoto) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;

        this.telf = telf;
        this.ciudad = ciudad;
        this.urlfoto = urlfoto;
    }
    public Contacto(int id, String nombre, String email, String urlfoto) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.urlfoto = urlfoto;
    }

   
    @Override
    public String toString() {
        return String.format("%s (%s)", this.nombre, this.email);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getUrlfoto(){
        return this.urlfoto;
    }
    
    public String getUrlfotoPath(){
        String base = "/contactos/uploads/";
        if (this.urlfoto==null || this.urlfoto.length()==0) {
            return base + "nofoto.png";
        } else {
            return base + this.urlfoto;
        }
    }

    protected void setUrlfoto(String urlfoto){
        this.urlfoto = urlfoto;
    }

    public String getTelf() {
        return telf;
    }

    public void setTelf(String telf) {
        this.telf = telf;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

}