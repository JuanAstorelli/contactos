package com.enfocat.mvc.businessLogic;

import java.util.Date;

public class Llamada {

    private int id;
    private int remitente;
    private int destinatario;
    private Date fecha;
    private String notas;

    public Llamada(int remitente, int destinatario, Date fecha, String notas) {

        this.id = -1;
        this.remitente = remitente;
        this.destinatario = destinatario;
        this.fecha = fecha;
        this.notas = notas;
    }
    
    public Llamada(int id, int remitente, int destinatario, Date fecha, String notas) {

        this.id = id;
        this.remitente = remitente;
        this.destinatario = destinatario;
        this.fecha = fecha;
        this.notas = notas;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRemitente() {
        return remitente;
    }

    public void setRemitente(int remitente) {
        this.remitente = remitente;
    }

    public int getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(int destinatario) {
        this.destinatario = destinatario;
    }
}